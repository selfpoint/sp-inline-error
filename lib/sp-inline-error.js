(function (angular) {
    function provider() {
        var self = this;

        self.direction = 'ltr';
        self.stickTo = 'right';
        self.errorGenerator = function (error) {
            if (!angular.isObject(error)) {
                return error;
            }

            return error.$name + ' ' + Object.keys(error.$error)[0];
        };

        self.$get = function () {
            return self;
        };
    }

    function drv(spInlineError, $filter, $rootScope) {
        return {
            restrict: 'A',
            link: _link
        };

        function _link($scope, $element, $attrs) {
            var _listeners = [],
                $modelCtrl = $element.data('$ngModelController'),
                $errorElement,
                _elementParentElement,
                _errorElementListener;

            _addErrorElement();

            if ($modelCtrl) {
                $element.bind('blur', _checkErrorByModel);

                _listeners.push($scope.$watch(function () {
                    return $modelCtrl.$invalid;
                }, function () {
                    if (!$modelCtrl.$touched) return;

                    _checkErrorByModel();
                }));

                _listeners.push($scope.$on('$destroy', function () {
                    $element.unbind('blur', _checkErrorByModel);
                }));
            } else {
                _listeners.push($scope.$watch(function () {
                    return $attrs['spInlineError'];
                }, function (val) {
                    _hideError();

                    if (val) {
                        _showError($attrs['spInlineError']);
                    }
                }));
            }

            function _addErrorElement() {
                var errorClass = 'sp-inline-error';
                var elementId
                if ($attrs.spInlineErrorClass) {
                    errorClass += ' ' + $attrs.spInlineErrorClass;
                }
                $errorElement = angular.element(document.createElement('div'))
                    .addClass(errorClass);

                $errorElement[0].setAttribute('role', 'alert');
                if($attrs.id){
                    elementId = errorClass + '-id_' + $attrs.id;
                    $errorElement[0].setAttribute('id', elementId);

                    var inputElement = $attrs.$$element[0];
                    if (inputElement) {
                        inputElement.setAttribute('aria-describedby', elementId);
                    }
                }

                _elementParentElement = null;
                if ($attrs.spInlineErrorAttachToParent) {
                    var parentElement = $element[0];
                    do {
                        parentElement = parentElement.parentElement;
                    } while (parentElement.tagName.toUpperCase() != $attrs.spInlineErrorAttachToParent.toUpperCase() && parentElement.parentElement);
                    _elementParentElement = parentElement;
                }

                var toAppendParent = _elementParentElement || $element[0];
                do {
                    toAppendParent = toAppendParent.offsetParent;
                } while (!!toAppendParent && !_isElementFixed(toAppendParent));

                if (!!toAppendParent) {
                    angular.element(toAppendParent).append($errorElement);
                } else {
                    document.body.appendChild($errorElement[0]);
                }
                
                $rootScope.$emit('spInlineError.element.added', {
                    elementId: elementId,
                    attrId: $attrs.id
                });
            }

            function _checkErrorByModel() {
                if ($modelCtrl.$invalid) {
                    _showError($modelCtrl);
                } else {
                    _hideError();
                }
            }

            function _showError(error) {
                _hideError();

                if ($attrs.spInlineErrorAttachToParent && _elementParentElement) {
                    angular.element(_elementParentElement).addClass('sp-inline-error-invalid');
                }

                $element.addClass('sp-inline-error-invalid');
                $errorElement
                    .text(spInlineError.errorGenerator(error, $filter))
                    .addClass(spInlineError.direction + ' shown');
                $errorElement.css(_errorPosition());

                $errorElement.bind('click', function(event) {
                    event.stopPropagation();
                    return false;
                });

                _errorElementListener = $rootScope.$watch(function() {
                    setTimeout(function() {
                        var locationProps = $element[0].getBoundingClientRect();
                        if (locationProps.width || locationProps.bottom || locationProps.left || locationProps.right || locationProps.bottom) return;

                        _hideError();
                    });
                });
            }

            function _isElementFixed(element) {
                element = angular.element(element)[0];
                var res = '';
                if (window.getComputedStyle && angular.isFunction(window.getComputedStyle)) {
                    res = window.getComputedStyle(element).getPropertyValue('position');
                } else if (element.currentStyle && element.currentStyle.position) {
                    res = element.currentStyle.position;
                }
                return res == 'fixed' || res == 'relative' || res == 'absolute';
            }


            function _errorPosition() {
                var element = _elementParentElement || $element[0],
                    errorElementRect = $errorElement[0].getBoundingClientRect();

                var toAppendParent = element,
                    top = 0,
                    left = 0;
                do {
                    top += toAppendParent.offsetTop;
                    left += toAppendParent.offsetLeft;
                    toAppendParent = toAppendParent.offsetParent;
                } while (!!toAppendParent && !_isElementFixed(toAppendParent));

                var res = {
                    top: (top + ((element.offsetHeight - errorElementRect.height) / 2)) + 'px'
                };

                if (spInlineError.stickTo === 'right') {
                    if (spInlineError.direction == 'ltr') {
                        res.left = (left + element.offsetWidth + 10) + 'px';
                    } else {
                        res.left = (left - errorElementRect.width - 10) + 'px';
                    }
                }

                if (spInlineError.stickTo === 'bottom') {
                    res.top = (top + element.offsetHeight + 1) + 'px';
                    res.left = left + 'px';
                    res.width = element.offsetWidth + 'px';
                }

                return res;
            }

            function _hideError() {
                if ($errorElement) {
                    $element.removeClass('sp-inline-error-invalid');
                    $errorElement
                        .removeClass('shown')
                        .removeClass(spInlineError.direction)
                        .text('');
                }
                if (_elementParentElement) {
                    angular.element(_elementParentElement).removeClass('sp-inline-error-invalid');
                }
            }

            function _removeError() {
                if (_errorElementListener) {
                    _errorElementListener();
                    _errorElementListener = null;
                }

                if ($errorElement) {
                    $errorElement.remove();
                    $errorElement = null;
                }

                if (_elementParentElement) {
                    _elementParentElement = null;
                }
            }

            _listeners.push($rootScope.$on('spInlineError.resize', function (){
                if ($errorElement) {
                    $errorElement.css(_errorPosition());
                }
            }));

            _listeners.push($rootScope.$on('spInlineError.clear', function (){
                _hideError();
            }));

            _listeners.push($scope.$on('$destroy', function () {
                _removeError();
                angular.forEach(_listeners, function (listener) {
                    listener();
                });
            }));
        }
    }

    angular.module('spInlineError', [])
        .provider('spInlineError', provider)
        .directive('spInlineError', ['spInlineError', '$filter', '$rootScope', drv])
        .run(['$rootScope', function ($rootScope) {
            window.addEventListener('resize', function (event) {
                $rootScope.$emit('spInlineError.resize', event);
            });
        }]);
})(angular);