'use strict';

const gulp = require('gulp'),
    git = require('gulp-git'),
    sass = require('gulp-sass'),
    rename = require("gulp-rename"),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    minifyCss = require('gulp-clean-css'),
    paths = {
        js: 'lib/*.js',
        sass: 'lib/*.scss'
    };

const dist = gulp.series(_distJs, _distSass);

module.exports = {
    default: dist,
    dist
}

function _distJs() {
    return gulp.src(paths.js)
        .pipe(concat('sp-inline-error.js'))
        .pipe(gulp.dest('dist'))
        .pipe(git.add())
        .pipe(uglify({
            compress: true
        }))
        .pipe(rename('sp-inline-error.min.js'))
        .pipe(gulp.dest('dist'))
        .pipe(git.add());
}

function _distSass() {
    return gulp.src(paths.sass)
        .pipe(sass())
        .pipe(concat('sp-inline-error.css'))
        .pipe(gulp.dest('dist'))
        .pipe(git.add())
        .pipe(minifyCss({specialComments: 0}))
        .pipe(rename('sp-inline-error.min.css'))
        .pipe(gulp.dest('dist'))
        .pipe(git.add());
}